/**
 * @file  vl53l1_platform.h
 * @brief Those platform functions are platform dependent and have to be implemented by the user
 */

#ifndef _VL53L1_PLATFORM_H_
#define _VL53L1_PLATFORM_H_

#include "vl53l1_types.h"
#include "vl53l1_error_codes.h"

#ifdef __cplusplus
extern "C"
{
#endif

#define I2C_TIMEOUT_BASE   10
#define I2C_TIMEOUT_BYTE   1

/**
 * @brief Write data to device using I2C bus
 *
 * @param[in] dev device address
 * @param[in] pData pointer to buffer containing data to be sent
 * @param[in] count number of bytes in buffer
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error _VL53L1_I2C_Write(uint16_t dev, uint8_t *pData, uint32_t count);

/**
 * @brief Read data from device using I2C bus
 *
 * @param[in] dev device address
 * @param[out] pData pointer to buffer to store read data
 * @param[in] count number of bytes to read
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error _VL53L1_I2C_Read(uint16_t dev, uint8_t *pData, uint32_t count);

/**
 * @brief Write given byte array to device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[in] pData pointer to buffer containing data to be sent
 * @param[in] count number of bytes in buffer
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_WriteMulti(uint16_t dev, uint16_t index, uint8_t *pData,
		uint32_t count);

/**
 * @brief Read requested number of bytes from device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[out] pData pointer to buffer to store read data
 * @param[in] count number of bytes to read
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_ReadMulti(uint16_t dev, uint16_t index, uint8_t *pData,
		uint32_t count);

/**
 * @brief Write single byte to device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[in] data data value to write
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_WrByte(uint16_t dev, uint16_t index, uint8_t data);

/**
 * @brief Read single byte from device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[out] pData pointer to buffer to store read data
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_RdByte(uint16_t dev, uint16_t index, uint8_t *pData);

/**
 * @brief Write single word to device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[in] data data value to write
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_WrWord(uint16_t dev, uint16_t index, uint16_t data);

/**
 * @brief Read a single word from device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[out] pData pointer to buffer to store read data
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_RdWord(uint16_t dev, uint16_t index, uint16_t *pData);

/**
 * @brief Write single dword to device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[in] data data value to write
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_WrDWord(uint16_t dev, uint16_t index, uint32_t data);

/**
 * @brief Read a single dword from device
 *
 * @param[in] dev device address
 * @param[in] index register index value
 * @param[out] pData pointer to buffer to store read data
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_RdDWord(uint16_t dev, uint16_t index, uint32_t *pData);

/**
 * @brief Implements waint in ms
 *
 * @param[in] dev device address
 * @param[in] wait_ms time to wait in miliseconds
 *
 * @return VL53L1_ERROR_NONE - Success
 * @return VL53L1_ERROR_* - See vl53l1_error_codes.h
 */
VL53L1_Error VL53L1_WaitMs(uint16_t dev, int32_t wait_ms);

#ifdef __cplusplus
}
#endif

#endif
