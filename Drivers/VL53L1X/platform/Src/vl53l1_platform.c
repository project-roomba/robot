/* 
 * This file is part of VL53L1 Platform 
 * 
 * Copyright (c) 2016, STMicroelectronics - All Rights Reserved 
 * 
 * License terms: BSD 3-clause "New" or "Revised" License. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this 
 * list of conditions and the following disclaimer. 
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 * this list of conditions and the following disclaimer in the documentation 
 * and/or other materials provided with the distribution. 
 * 
 * 3. Neither the name of the copyright holder nor the names of its contributors 
 * may be used to endorse or promote products derived from this software 
 * without specific prior written permission. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. 
 * 
 */

#include "vl53l1_platform.h"
#include "vl53l1_error_codes.h"
#include "i2c.h"

#include <string.h>
#include <time.h>
#include <math.h>

uint8_t _I2CBuffer[256];

VL53L1_Error _VL53L1_I2C_Write(uint16_t dev, uint8_t *pData, uint32_t count) {
	VL53L1_Error status = VL53L1_ERROR_NONE;
	uint32_t i2c_timeout = I2C_TIMEOUT_BASE + count * I2C_TIMEOUT_BYTE;

	// TODO: Adapt to proper I2C configuration
	status = HAL_I2C_Master_Transmit(&hi2c1, dev, pData, count, i2c_timeout);
	if (status != 0) {
		// TODO: Add error handler
	}

	return status;
}

VL53L1_Error _VL53L1_I2C_Read(uint16_t dev, uint8_t *pData, uint32_t count) {
	VL53L1_Error status = VL53L1_ERROR_NONE;
	uint32_t i2c_timeout = I2C_TIMEOUT_BASE + count * I2C_TIMEOUT_BYTE;

	// TODO: Adapt to proper I2C configuration
	status = HAL_I2C_Master_Receive(&hi2c1, dev, pData, count, i2c_timeout);
	if (status != 0) {
		// TODO: Add error handler
	}

	return status;
}

VL53L1_Error VL53L1_WriteMulti(uint16_t dev, uint16_t index, uint8_t *pData,
		uint32_t count) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	if (count > sizeof(_I2CBuffer) - 1) {
		return VL53L1_ERROR_INVALID_PARAMS;
	}

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;
	memcpy(&_I2CBuffer[2], pData, count);

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, count + 2);

	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_ReadMulti(uint16_t dev, uint16_t index, uint8_t *pData,
		uint32_t count) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 2);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	status = _VL53L1_I2C_Read(dev, pData, count);
	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_WrByte(uint16_t dev, uint16_t index, uint8_t data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;
	_I2CBuffer[2] = data;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 3);
	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_RdByte(uint16_t dev, uint16_t index, uint8_t *data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 2);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	status = _VL53L1_I2C_Read(dev, data, 1);
	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_WrWord(uint16_t dev, uint16_t index, uint16_t data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;
	_I2CBuffer[2] = data >> 8;
	_I2CBuffer[3] = data & 0x00FF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 4);
	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_RdWord(uint16_t dev, uint16_t index, uint16_t *data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 2);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	status = _VL53L1_I2C_Read(dev, _I2CBuffer, 2);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	*data = ((uint16_t) _I2CBuffer[0] << 8) + (uint16_t) _I2CBuffer[1];

	return status;
}

VL53L1_Error VL53L1_WrDWord(uint16_t dev, uint16_t index, uint32_t data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;
	_I2CBuffer[2] = (data >> 24) & 0xFF;
	_I2CBuffer[3] = (data >> 16) & 0xFF;
	_I2CBuffer[4] = (data >> 8) & 0xFF;
	_I2CBuffer[5] = (data >> 0) & 0xFF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 6);
	if (status != 0) {
		status = VL53L1_ERROR_CONTROL_INTERFACE;
	}

	return status;
}

VL53L1_Error VL53L1_RdDWord(uint16_t dev, uint16_t index, uint32_t *data) {
	VL53L1_Error status = VL53L1_ERROR_NONE;

	_I2CBuffer[0] = index >> 8;
	_I2CBuffer[1] = index & 0xFF;

	status = _VL53L1_I2C_Write(dev, _I2CBuffer, 2);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	status = _VL53L1_I2C_Read(dev, _I2CBuffer, 4);
	if (status != 0) {
		return VL53L1_ERROR_CONTROL_INTERFACE;
	}

	*data = ((uint32_t) _I2CBuffer[0] << 24) + ((uint32_t) _I2CBuffer[1] << 16)
			+ ((uint32_t) _I2CBuffer[2] << 8) + (uint32_t) _I2CBuffer[3];

	return status;
}

VL53L1_Error VL53L1_WaitMs(uint16_t dev, int32_t wait_ms) {
	(void) dev;

	HAL_Delay(wait_ms);

	return VL53L1_ERROR_NONE;
}
