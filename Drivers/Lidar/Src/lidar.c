/**
 * @file lidar.c
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Source file for own lidar driver
 * @version 1.0
 * @date 2022-05-03
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#include "lidar.h"

void LIDAR_Init(LIDAR_handle *handle, TIM_HandleTypeDef *pulse_timer,
		uint32_t channel, TIM_HandleTypeDef *steps_timer,
		uint16_t laser_address) {
	handle->direction = CW;
	handle->to_measure = 0;
	handle->angle = 0;
	handle->dev = laser_address;

	DRV8834_Init(&(handle->stepper), pulse_timer, channel, steps_timer);

	VL53L1X_SensorInit(handle->dev);
	VL53L1X_SetDistanceMode(handle->dev, 2); // Set long mode
	VL53L1X_SetTimingBudgetInMs(handle->dev, 20); // Set smallest available time
	VL53L1X_SetInterMeasurementInMs(handle->dev, 50); // IM >= TB

	HAL_Delay(2);
}

void LIDAR_Measurement(LIDAR_handle *handle, LIDAR_data *measurement) {
	uint8_t data_ready;
	uint16_t distance;
	int status;

	if (!handle->to_measure) {
		DRV8834_RunAngle(&(handle->stepper), handle->direction, LIDAR_SPEED,
		LIDAR_STEP_ANGLE);
		handle->angle += 1;
	} else {
		status = VL53L1X_StartRanging(handle->dev);
		//printf("%d\r\n", status);
		while (data_ready == 0) {
			VL53L1X_CheckForDataReady(handle->dev, &data_ready);
			HAL_Delay(2);
		}
		//printf("Data ready\r\n");
		data_ready = 0;
		VL53L1X_GetDistance(handle->dev, &distance);
		VL53L1X_ClearInterrupt(handle->dev);
		VL53L1X_StopRanging(handle->dev);

		handle->to_measure = 0;

		measurement->distance = distance;
		measurement->angle = handle->angle;

//		printf("%d %d | ", measurement->distance, measurement->angle);
	}
}

void LIDAR_TIM_Callback(LIDAR_handle *handle) {
	DRV8834_Stop(&handle->stepper);
	handle->to_measure = 1;

}

void LIDAR_EXTI_Callback(LIDAR_handle *handle) {
	handle->angle = 0;
	handle->direction = !handle->direction;

}
