/**
 * @file lidar.h
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Header file for own lidar driver
 * @version 1.0
 * @date 2022-05-03
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#ifndef LIDAR_H_
#define LIDAR_H_

#include "DRV8834.h"
#include "VL53L1X_api.h"

/*
 * IMPORTANT!
 * Empirically selected values. DO NOT change, otherwise behavior is unpredictable.
 */
#define LIDAR_SPEED 10
#define LIDAR_STEP_ANGLE 1.8

typedef struct {
	uint8_t angle;
	uint16_t distance;
} LIDAR_data;

typedef struct lidar_s {
	uint16_t dev;
	DRV8834_struct stepper;
	volatile DRV8834_direction direction;
	volatile uint8_t angle;
	volatile uint8_t to_measure;
} LIDAR_handle;

/**
 * @brief Initialize Lidar sensor
 * @details Initialize periferials and rotate lidar to it's starting position
 * @param[inout] handle Lidar handle structure
 * @param[in] pulse_timer Timer which generates PWM signal to rotate stepper motor
 * @param[in] channel PWM timer channel
 * @param[in] steps_timer Timer which counts PWM impulses
 * @param[in] laser_address I2C laser sensor address
 */
void LIDAR_Init(LIDAR_handle *handle, TIM_HandleTypeDef *pulse_timer,
		uint32_t channel, TIM_HandleTypeDef *steps_timer,
		uint16_t laser_address);

/**
 * @brief Make one step and one measurement per shift
 * @param[in] handle Lidar handle structure
 * @param[out] measurement Structure containing distance and angle
 */
void LIDAR_Measurement(LIDAR_handle *handle, LIDAR_data *measurement);

/**
 * @brief Steps timer period elapsed callback
 * @param[in] handle Lidar handle structure
 */
void LIDAR_TIM_Callback(LIDAR_handle *handle);

/**
 * @brief Hall sensor interupt callback
 * @details Hall sensor is used to position laser sensor in starting position.
 * LIDAR_Init positions sensor and rotate it until external interupt occures.
 * @param[in] handle Lidar handle structure
 */
void LIDAR_EXTI_Callback(LIDAR_handle *handle);

#endif /* LIDAR_H_ */
