/**
 * @file DRV8834.h
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Header file for DRV8834 stepper motor driver drivers
 * @version 1.0
 * @date 2022-04-24
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#ifndef DRV8834_H_
#define DRV8834_H_

#include "main.h"

#define STEPPER_MOTOR_MAX_FREQ_HZ	(DRV8834_MICROSTEPS * 1000)
#define STEPPER_MOTOR_MIN_FREQ_HZ	1

#define STEPPER_MOTOR_MAX_SPEED		100

#define DRV8834_STEPS_PER_REVOLUTION 200
#define DRV8834_MICROSTEPS 1

typedef enum {
	CCW = 0, CW = 1
} DRV8834_direction;

typedef struct htim_s {
	TIM_HandleTypeDef *htim;
	uint32_t channel;
} DRV8834_htim;

typedef struct stepper_s {
	DRV8834_htim timer;
	DRV8834_htim slave_timer;
} DRV8834_struct;

/**
 * @brief Initialize DRV8344 struct handler
 * @param[inout] stepper structure handler
 * @param[in] timer main timer with pwm output which control stepper driver
 * @param[in] channel channel on which pwm has output
 * @param[in] slave_timer timer to count pwm impulses
 */
void DRV8834_Init(DRV8834_struct *stepper, TIM_HandleTypeDef *timer,
		uint32_t channel, TIM_HandleTypeDef *slave_timer);

/**
 * @brief Start step motor in continous mode
 * @param[in] stepper structure handler
 * @param[in] direction direction in which motor will spin
 * @param[in] speed value of speed
 */
void DRV8834_RunContinous(DRV8834_struct *stepper, DRV8834_direction direction,
		uint32_t speed);

/**
 * @brief Rotate motor of given angle
 * @param[in] stepper structure handler
 * @param[in] direction direction in which motor will spin
 * @param[in] speed value of speed
 * @param[in] angle value of angle to rotate
 */
void DRV8834_RunAngle(DRV8834_struct *stepper, DRV8834_direction direction,
		uint32_t speed, float angle);

/**
 * @brief Stop motor
 * @param[in] stepper structure handler
 */
void DRV8834_Stop(DRV8834_struct *stepper);

/**
 * @brief Set rotation direction
 * @param[in] stepper structure handler
 * @param[in] direction direction in which motor will spin
 */
void DRV8834_SetDirection(DRV8834_struct *stepper, DRV8834_direction direction);

/**
 * @brief Set rotation speed
 * @param[in] stepper structure handler
 * @param[in] speed value of speed
 */
void DRV8834_SetSpeed(DRV8834_struct *stepper, uint32_t speed);

/**
 * @brief Set number of steps after which the callback will be called
 * @param[in] steps number of steps
 */
void DRV8834_SetSteps(DRV8834_struct *stepper, uint32_t steps);

#endif /* DRV8843_H_ */
