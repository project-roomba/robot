/**
 * @file DRV8834.c
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Source file for DRV8834 stepper motor driver drivers
 * @version 1.0
 * @date 2022-04-24
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#include "DRV8834.h"

void DRV8834_Init(DRV8834_struct *stepper, TIM_HandleTypeDef *timer,
		uint32_t channel, TIM_HandleTypeDef *slave_timer) {
	stepper->timer.htim = timer;
	stepper->timer.channel = channel;
	stepper->slave_timer.htim = slave_timer;

	HAL_TIM_Base_Start_IT(stepper->slave_timer.htim);

	HAL_Delay(2);
}

void DRV8834_RunContinous(DRV8834_struct *stepper, DRV8834_direction direction,
		uint32_t speed) {
	DRV8834_SetDirection(stepper, direction);
	DRV8834_SetSpeed(stepper, speed);
	DRV8834_SetSteps(stepper, 0);

	HAL_TIM_PWM_Start(stepper->timer.htim, stepper->timer.channel);
}

void DRV8834_RunAngle(DRV8834_struct *stepper, DRV8834_direction direction,
		uint32_t speed, float angle) {
	DRV8834_SetDirection(stepper, direction);
	DRV8834_SetSpeed(stepper, speed);
	DRV8834_SetSteps(stepper,
			(DRV8834_STEPS_PER_REVOLUTION * DRV8834_MICROSTEPS * angle) / 360.0);

	HAL_TIM_PWM_Start(stepper->timer.htim, stepper->timer.channel);
}

void DRV8834_Stop(DRV8834_struct *stepper) {
	HAL_TIM_PWM_Stop(stepper->timer.htim, stepper->timer.channel);
}

void DRV8834_SetDirection(DRV8834_struct *stepper, DRV8834_direction direction) {
	if (direction == CCW) {
		HAL_GPIO_WritePin(SM_DIR_GPIO_Port, SM_DIR_Pin, CCW);
	} else if (direction == CW) {
		HAL_GPIO_WritePin(SM_DIR_GPIO_Port, SM_DIR_Pin, CW);
	}
}

void DRV8834_SetSpeed(DRV8834_struct *stepper, uint32_t speed) {
	uint32_t freq, pulse;

	if (speed > 100) {
		speed = 100;
	} else if (speed == 0) {
		DRV8834_Stop(stepper);
		return;
	}

	freq = HAL_RCC_GetPCLK1Freq()
			/ (((stepper->timer.htim->Init.Prescaler + 1) * speed
					* DRV8834_STEPS_PER_REVOLUTION * DRV8834_MICROSTEPS) / 60)
			- 1;
	pulse = freq / 2;

	__HAL_TIM_SET_AUTORELOAD(stepper->timer.htim, freq);
	__HAL_TIM_SET_COMPARE(stepper->timer.htim, stepper->timer.channel, pulse);
}

void DRV8834_SetSteps(DRV8834_struct *stepper, uint32_t steps) {
	__HAL_TIM_SET_AUTORELOAD(stepper->slave_timer.htim, steps);
}

