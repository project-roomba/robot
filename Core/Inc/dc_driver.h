/*
 * dc_driver.h
 *
 *  Created on: Apr 25, 2022
 *      Author: bartosz
 */

#ifndef INC_DC_DRIVER_H_
#define INC_DC_DRIVER_H_

#include "tim.h"
#include "gpio.h"

#define LOW_LIMIT_SPEED 4100		//LOWEST VALUE OF PWM DURING MOVE
#define HIGH_LIMIT_SPEED 9999		//HIGHEST VALUE OF PWM DURING MOVE

#define F_FACTOR	-2.87			//EQUATION FACTOR DURING FORWARD MOVE
#define F_CONST		9999			//EQUATION CONSTANT DURING FORWARD MOVE

#define B_FACTOR	3.06			//EQUATION FACTOR DURING BACKWARD MOVE
#define B_CONST		-2546.33		//EQUATION CONSTANT DURING BACKWARD MOVE

#define L_FACTOR	-2.92			//EQUATION FACTOR DURING TURNING LEFT IN PLACE
#define L_CONST		9999			//EQUATION CONSTANT DURING TURNING LEFT IN PLACE

#define R_FACTOR	2.99			//EQUATION FACTOR DURING TURNING RIGHT IN PLACE
#define R_CONST		-2064.62		//EQUATION CONSTANT DURING TURNING RIGHT IN PLACE

//#define L_JOY_LIMIT_X	1950		//JOYSTICK DEFAULT LOWER LIMIT IN AXIS X (DATA READ FROM ANALOG INPUT)
//#define H_JOY_LIMIT_X	2100		//JOYSTICK DEFAULT HIGH LIMIT IN AXIS X (DATA READ FROM ANALOG INPUT)
//#define L_JOY_LIMIT_Y	2000		//JOYSTICK DEFAULT LOWER LIMIT IN AXIS Y (DATA READ FROM ANALOG INPUT)
//#define H_JOY_LIMIT_Y	2150		//JOYSTICK DEFAULT HIGH LIMIT IN AXIS Y (DATA READ FROM ANALOG INPUT)

#define L_JOY_LIMIT_X	2000		//JOYSTICK DEFAULT LOWER LIMIT IN AXIS X (DATA READ FROM ANALOG INPUT)
#define H_JOY_LIMIT_X	2150		//JOYSTICK DEFAULT HIGH LIMIT IN AXIS X (DATA READ FROM ANALOG INPUT)
#define L_JOY_LIMIT_Y	1950		//JOYSTICK DEFAULT LOWER LIMIT IN AXIS Y (DATA READ FROM ANALOG INPUT)
#define H_JOY_LIMIT_Y	2100		//JOYSTICK DEFAULT HIGH LIMIT IN AXIS Y (DATA READ FROM ANALOG INPUT)

typedef struct motor_dc {
	int speed;			//0-9999
	uint8_t IN_1;			//0-1
	uint8_t IN_2;			//0-1
} Motor_DC;

void dc_control(Motor_DC *motor1, Motor_DC *motor2, volatile uint16_t *joystick);
void fast_stop(Motor_DC *motor);
void soft_stop(Motor_DC *motor);
uint8_t check_rotation(Motor_DC *motor);

#endif /* INC_DC_DRIVER_H_ */
