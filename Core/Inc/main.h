/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.h
 * @brief          : Header for main.c file.
 *                   This file contains the common defines of the application.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define B1_Pin GPIO_PIN_13
#define B1_GPIO_Port GPIOC
#define B1_EXTI_IRQn EXTI15_10_IRQn
#define ADC1_IN1_Pin GPIO_PIN_0
#define ADC1_IN1_GPIO_Port GPIOC
#define SPI2_MISO_Pin GPIO_PIN_2
#define SPI2_MISO_GPIO_Port GPIOC
#define SPI2_MOSI_Pin GPIO_PIN_3
#define SPI2_MOSI_GPIO_Port GPIOC
#define USART_TX_Pin GPIO_PIN_2
#define USART_TX_GPIO_Port GPIOA
#define USART_RX_Pin GPIO_PIN_3
#define USART_RX_GPIO_Port GPIOA
#define NSS_Pin GPIO_PIN_4
#define NSS_GPIO_Port GPIOA
#define LD2_Pin GPIO_PIN_5
#define LD2_GPIO_Port GPIOA
#define DC_MOTOR_IN_4_Pin GPIO_PIN_4
#define DC_MOTOR_IN_4_GPIO_Port GPIOC
#define DC_MOTOR_IN_3_Pin GPIO_PIN_5
#define DC_MOTOR_IN_3_GPIO_Port GPIOC
#define DIO0_Pin GPIO_PIN_0
#define DIO0_GPIO_Port GPIOB
#define DIO0_EXTI_IRQn EXTI0_IRQn
#define RESET_Pin GPIO_PIN_1
#define RESET_GPIO_Port GPIOB
#define SM_DIR_Pin GPIO_PIN_10
#define SM_DIR_GPIO_Port GPIOB
#define SPI2_SCK_Pin GPIO_PIN_13
#define SPI2_SCK_GPIO_Port GPIOB
#define GPIO_EXTI14_Pin GPIO_PIN_14
#define GPIO_EXTI14_GPIO_Port GPIOB
#define GPIO_EXTI14_EXTI_IRQn EXTI15_10_IRQn
#define GPIO_EXTI15_Pin GPIO_PIN_15
#define GPIO_EXTI15_GPIO_Port GPIOB
#define GPIO_EXTI15_EXTI_IRQn EXTI15_10_IRQn
#define DC_MOTOR_IN_2_Pin GPIO_PIN_6
#define DC_MOTOR_IN_2_GPIO_Port GPIOC
#define DC_MOTOR_IN_1_Pin GPIO_PIN_7
#define DC_MOTOR_IN_1_GPIO_Port GPIOC
#define TIM3_CH3_Pin GPIO_PIN_8
#define TIM3_CH3_GPIO_Port GPIOC
#define TIM3_CH4_Pin GPIO_PIN_9
#define TIM3_CH4_GPIO_Port GPIOC
#define SM_STEP_Pin GPIO_PIN_8
#define SM_STEP_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define SWO_Pin GPIO_PIN_3
#define SWO_GPIO_Port GPIOB
#define GPIO_EXTI_5_Pin GPIO_PIN_5
#define GPIO_EXTI_5_GPIO_Port GPIOB
#define GPIO_EXTI_5_EXTI_IRQn EXTI9_5_IRQn
#define I2C1_SCL_Pin GPIO_PIN_6
#define I2C1_SCL_GPIO_Port GPIOB
#define I2C1_SDA_Pin GPIO_PIN_7
#define I2C1_SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
