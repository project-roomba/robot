/* USER CODE BEGIN Header */
/**
 ****************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ****************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ****************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "crc.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "dc_driver.h"
#include "SX1278.h"
#include "lidar.h"
#include "communication.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define LINE_MAX_LENGTH	80
#define PI 3.1416
#define WHEEL PI*6.5		//circumference of a circle [cm]
#define MAX_LORA_TIME_WAIT 100
#define MAX_TIME_SPEED_WAITING 1000
#define BATERRY_FACTOR	0.34			//EQUATION FACTOR FOR BATERRY EQUATION
#define BATERRY_CONST	685		//EQUATION CONSTANT FOR BATERRY EQUATION

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

volatile uint32_t actual_time_DC_1;
volatile uint32_t prev_time_DC_1;
volatile uint32_t actual_time_DC_2;
volatile uint32_t prev_time_DC_2;

volatile int speed_DC_1 = 10, speed_DC_2 = 10, prev_speed_DC_1 = 7,
		prev_speed_DC_2 = 7;
volatile int accel_r_motor = 5, accel_l_motor = 5;

volatile static uint16_t baterry;
uint16_t calculated_baterry;
uint16_t joystick[2];

SX1278_hw_t SX1278_hw;
SX1278_t SX1278;

int ret;
char buffer[64];  //used 43
int message;
int message_length;
volatile _Bool reciev = true;
char tmp[8];

uint16_t laser_dist[200];
uint16_t laser_angle[200];
uint16_t CRC16 = 0;

Motor_DC DC_1, DC_2;
uint32_t last_time_motor_measur = 0;

LIDAR_handle lidar = { 0 };
LIDAR_data lidar_data_measurement = { 0 };
LIDAR_Data lidar_measurement = { 0 };

uint8_t tmp_com = 0;

volatile _Bool calibration = false;
char pc_frame[PC_FRAME_LENGTH];
PC_FrameData pc_frame_data = { 0 };
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
int __io_putchar(int ch) {
	HAL_UART_Transmit(&huart2, (uint8_t*) &ch, 1, HAL_MAX_DELAY);
	return 1;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin) {

	actual_time_DC_1 = HAL_GetTick();
	actual_time_DC_2 = HAL_GetTick();

	if (GPIO_Pin == GPIO_EXTI15_Pin) {
		if (actual_time_DC_1 - prev_time_DC_1 > 800) {
			prev_speed_DC_1 = speed_DC_1;
			speed_DC_1 = WHEEL / (actual_time_DC_1 - prev_time_DC_1) * 1000;
			accel_l_motor = (speed_DC_1 - prev_speed_DC_1)
					/ (actual_time_DC_1 / 1000 - prev_time_DC_1 / 1000);
			prev_time_DC_1 = HAL_GetTick();
		}
	}

	if (GPIO_Pin == GPIO_EXTI14_Pin) {
		if (actual_time_DC_2 - prev_time_DC_2 > 800) {
			prev_speed_DC_2 = speed_DC_2;
			speed_DC_2 = WHEEL / (double) (actual_time_DC_2 - prev_time_DC_2)
					* 1000;
			accel_r_motor = (speed_DC_2 - prev_speed_DC_2)
					/ (actual_time_DC_2 / 1000 - prev_time_DC_2 / 1000);
			prev_time_DC_2 = HAL_GetTick();
		}
	}

	if (GPIO_Pin == GPIO_EXTI_5_Pin) {
		LIDAR_EXTI_Callback(&lidar);
		printf("Przerwanie od czujnika hall'a z lasera. \r\n");
		calibration = true;
	}

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim->Instance == lidar.stepper.slave_timer.htim->Instance) {
		LIDAR_TIM_Callback(&lidar);
	}
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void) {
	/* USER CODE BEGIN 1 */
	uint8_t final_angle = 0;
	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_SPI2_Init();
	MX_TIM2_Init();
	MX_TIM1_Init();
	MX_DMA_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	MX_I2C1_Init();
	MX_CRC_Init();
	/* USER CODE BEGIN 2 */

	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_4);

	HAL_ADCEx_Calibration_Start(&hadc1, ADC_SINGLE_ENDED);
	HAL_ADC_Start_DMA(&hadc1, (uint32_t*) &baterry, 1);

	LIDAR_Init(&lidar, &htim1, TIM_CHANNEL_1, &htim2, 0x52);

	SX1278_hw.dio0.port = DIO0_GPIO_Port;
	SX1278_hw.dio0.pin = DIO0_Pin;
	SX1278_hw.nss.port = NSS_GPIO_Port;
	SX1278_hw.nss.pin = NSS_Pin;
	SX1278_hw.reset.port = RESET_GPIO_Port;
	SX1278_hw.reset.pin = RESET_Pin;
	SX1278_hw.spi = &hspi2;
	SX1278.hw = &SX1278_hw;

	SX1278_init(&SX1278, 434000000, SX1278_POWER_17DBM, SX1278_LORA_SF_7,
	SX1278_LORA_BW_125KHZ, SX1278_LORA_CR_4_5, SX1278_LORA_CRC_EN, 255);
	SX1278_LoRaEntryRx(&SX1278, 16, 2000);

	while (calibration == false)
		DRV8834_RunContinous(&(lidar.stepper), lidar.direction, 10);
	DRV8834_Stop(&(lidar.stepper));

	while (tmp_com != 11) {
		HAL_Delay(50);
		ret = SX1278_LoRaRxPacket(&SX1278);
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		if (ret > 0) {
			tmp_com = SX1278_read(&SX1278, (uint8_t*) buffer, ret);
		}
	}

	strncpy(tmp, buffer + 2, 4);
	joystick[0] = strtol(tmp, NULL, 16);

	strncpy(tmp, buffer + 7, 4);
	joystick[1] = strtol(tmp, NULL, 16);

	PCCOM_Init(&pc_frame_data);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {

		if (HAL_GetTick() - last_time_motor_measur > MAX_TIME_SPEED_WAITING) {
			speed_DC_1 = 0;
			speed_DC_2 = 0;
			accel_r_motor = 0;
			accel_l_motor = 0;
			last_time_motor_measur = HAL_GetTick();
		}

		dc_control(&DC_1, &DC_2, joystick);
		PCCOM_SetMotorsSpeed(&pc_frame_data, speed_DC_1, speed_DC_2);

		PCCOM_SetMotorsAcceleration(&pc_frame_data, accel_l_motor,
				accel_r_motor);
		PCCOM_SetJoystickSwing(&pc_frame_data, joystick[0], joystick[1]);

		LIDAR_Measurement(&lidar, &lidar_data_measurement);
		LIDAR_Measurement(&lidar, &lidar_data_measurement);
		;
		for (int i = 0; i < 3; i++) {
			if (lidar.direction == CCW) {
				final_angle = lidar_data_measurement.angle * 3 + i;
			} else {
				final_angle = 200 - lidar_data_measurement.angle * 3 - i;

			}
			lidar_measurement.angle = final_angle;
			lidar_measurement.distance = lidar_data_measurement.distance;
			PCCOM_AddOneLidarMeasurement(&pc_frame_data, lidar_measurement);
		}

		calculated_baterry = baterry * BATERRY_FACTOR - BATERRY_CONST;
		PCCOM_SetBatteryVoltage(&pc_frame_data, calculated_baterry);

		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
		ret = SX1278_LoRaRxPacket(&SX1278);

		if (ret == 11) {
			tmp_com = SX1278_read(&SX1278, (uint8_t*) buffer, ret);

			strncpy(tmp, buffer + 2, 4);
			joystick[0] = strtol(tmp, NULL, 16);

			strncpy(tmp, buffer + 7, 4);
			joystick[1] = strtol(tmp, NULL, 16);

		} else {
			soft_stop(&DC_1);
			soft_stop(&DC_2);
		}

		PCCOM_ConstructFrame(&pc_frame_data, pc_frame);
		printf("%s\r\n", pc_frame);
//		printf("Przyspieszenia - silnik lewy %d, silnik prawy %d \r\n",
//				accel_l_motor, accel_r_motor);

	}
	/* USER CODE END WHILE */

	/* USER CODE BEGIN 3 */

	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };

	/** Configure the main internal regulator output voltage
	 */
	if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1)
			!= HAL_OK) {
		Error_Handler();
	}

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 1;
	RCC_OscInitStruct.PLL.PLLN = 10;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
	RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
	RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK) {
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
