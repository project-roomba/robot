/**
 * @file communication.c
 * @author Jakub Kozłowicz <ja.kozlowicz@gmail.com>
 * @brief Source file for comunication with PC module
 * @version 1.0
 * @date 2022-05-14
 *
 * @copyright Copyright (c) 2022 Jakub Kozłowicz
 */

#include <stdio.h>
#include <string.h>

#include "crc.h"

#include "communication.h"

uint8_t PCCOM_Init(PC_FrameData *frame_data) {
	if (sprintf(frame_data->identifier, "%s", "X") != 1) {
		return -1;
	}

	frame_data->last_lidar_index = 0;
	frame_data->lidar_index_direction = 1;

	return 0;
}

uint8_t PCCOM_SetMotorsSpeed(PC_FrameData *frame_data, int16_t left_motor,
		int16_t right_motor) {
	frame_data->speed_left_motor = left_motor;
	frame_data->speed_right_motor = right_motor;

	return 0;
}

uint8_t PCCOM_SetMotorsAcceleration(PC_FrameData *frame_data,
		int16_t left_motor, int16_t right_motor) {
	frame_data->acceleration_left_motor = left_motor;
	frame_data->acceleration_right_motor = right_motor;

	return 0;
}

uint8_t PCCOM_SetJoystickSwing(PC_FrameData *frame_data, uint16_t x_axis,
		uint16_t y_axis) {
	frame_data->x_axis_joystick = x_axis;
	frame_data->y_axis_joystick = y_axis;

	return 0;
}

uint8_t PCCOM_SetBatteryVoltage(PC_FrameData *frame_data,
		uint16_t battery_volatage) {
	frame_data->battery_voltage = battery_volatage;

	return 0;
}

uint8_t PCCOM_AddOneLidarMeasurement(PC_FrameData *frame_data,
		LIDAR_Data lidar_data) {
	frame_data->lidar_table[frame_data->last_lidar_index] = lidar_data;
	frame_data->last_lidar_index += frame_data->lidar_index_direction;

	if (frame_data->last_lidar_index >= 200) {
		frame_data->lidar_index_direction = -1;
		frame_data->last_lidar_index += frame_data->lidar_index_direction;
	}
	else if(frame_data->last_lidar_index <= 14)
	{
		frame_data->lidar_index_direction = 1;
	}

	return 0;
}

uint8_t PCCOM_ConstructFrame(PC_FrameData *frame_data,
		char buffer[PC_FRAME_LENGTH]) {
	uint16_t crc;
	char crc_sign[5];
	char tmpLidar[11];

	if (sprintf(buffer, "%s %+05d %+05d %+05d %+05d %04d %04d %04d ",
			frame_data->identifier, frame_data->speed_left_motor,
			frame_data->speed_right_motor, frame_data->acceleration_left_motor,
			frame_data->acceleration_right_motor, frame_data->x_axis_joystick,
			frame_data->y_axis_joystick, frame_data->battery_voltage) != 41) {
		return -1;
	}

	for (uint8_t index = 0; index < 200; ++index) {
		if (sprintf(tmpLidar, "%04d %04d ",
				frame_data->lidar_table[index].angle,
				frame_data->lidar_table[index].distance) != 10) {
			return -1;
		}
		strcat(buffer, tmpLidar);
	}

	crc = HAL_CRC_Calculate(&hcrc, (uint32_t*) buffer, strlen(buffer));
	if (sprintf(crc_sign, "%04X", crc) != 4) {
		return -1;
	}

	strcat(buffer, crc_sign);
	return 0;
}
