/*
 * dc_driver.c
 *
 *  Created on: Apr 25, 2022
 *      Author: bartosz
 */

#include "dc_driver.h"

uint8_t check_rotation(Motor_DC *motor) {
	uint8_t result = 0;

	if (motor->IN_1 == 1 && motor->IN_2 == 0)
		result = 1;
	else if (motor->IN_1 == 0 && motor->IN_2 == 1)
		result = 0;

	return result;
}

void soft_stop(Motor_DC *motor) {
	motor->speed = 0;
	motor->IN_1 = 1;
	motor->IN_2 = 1;
}
void fast_stop(Motor_DC *motor) {
	motor->speed = 9999;
	motor->IN_1 = 1;
	motor->IN_2 = 1;
}

void dc_control(Motor_DC *motor1, Motor_DC *motor2, volatile uint16_t *joystick) {
	uint16_t y = joystick[0];
	uint16_t x = joystick[1];
	HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 0);

	if (x >= L_JOY_LIMIT_X && x <= H_JOY_LIMIT_X && y > H_JOY_LIMIT_Y) //TURNING LEFT IN PLACE
	{
		//printf("TURNING LEFT IN PLACE \r\n");
		motor1->speed = 0;
		motor2->speed = motor1->speed;

		motor1->IN_1 = 1;
		motor1->IN_2 = 0;
		motor2->IN_1 = 0;
		motor2->IN_2 = 1;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = L_FACTOR * y + L_CONST;
		motor2->speed = motor1->speed;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
	} else if (x >= L_JOY_LIMIT_X && x <= H_JOY_LIMIT_X && y < L_JOY_LIMIT_Y) //TURNING RIGHT IN PLACE
	{
		//printf("TURNING RIGHT IN PLACE \r\n");
		motor1->speed = 0;
		motor2->speed = motor1->speed;

		motor1->IN_1 = 0;
		motor1->IN_2 = 1;
		motor2->IN_1 = 1;
		motor2->IN_2 = 0;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = R_FACTOR * y + R_CONST;
		motor2->speed = motor1->speed;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
	} else if (x < L_JOY_LIMIT_X - 50 && y > H_JOY_LIMIT_Y) { //TURNING LEFT DURING DRIVING FORWARD

		//printf("TURNING LEFT DURING DRIVING FORWARD \r\n");

		motor1->IN_1 = 0;
		motor1->IN_2 = 1;
		motor2->IN_1 = 0;
		motor2->IN_2 = 1;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = LOW_LIMIT_SPEED;
		motor2->speed = HIGH_LIMIT_SPEED;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
		HAL_Delay(10);

	} else if (x < L_JOY_LIMIT_X - 50 && y < L_JOY_LIMIT_Y)	//TURNING RIGHT DURING DRIVING FORWARD
	{
		//printf("TURNING RIGHT DURING DRIVING FORWARD \r\n");


		motor1->IN_1 = 0;
		motor1->IN_2 = 1;
		motor2->IN_1 = 0;
		motor2->IN_2 = 1;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = HIGH_LIMIT_SPEED;
		motor2->speed = LOW_LIMIT_SPEED;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
		HAL_Delay(10);
	} else if (x > H_JOY_LIMIT_X + 50 && y > H_JOY_LIMIT_Y)	//TURNING LEFT DURING DRIVING BACKWARD
	{
		//printf("TURNING LEFT DURING DRIVING BACKWARD \r\n");


		motor1->IN_1 = 1;
		motor1->IN_2 = 0;
		motor2->IN_1 = 1;
		motor2->IN_2 = 0;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = LOW_LIMIT_SPEED;
		motor2->speed = HIGH_LIMIT_SPEED;

		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
		HAL_Delay(10);

	} else if (x > H_JOY_LIMIT_X + 50 && y < L_JOY_LIMIT_Y)	//TURNING RIGHT DURING DRIVING BACKWARD
	{
		//printf("TURNING RIGHT DURING DRIVING BACKWARD \r\n");


		motor1->IN_1 = 1;
		motor1->IN_2 = 0;
		motor2->IN_1 = 1;
		motor2->IN_2 = 0;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		motor1->speed = HIGH_LIMIT_SPEED;
		motor2->speed = LOW_LIMIT_SPEED;

		HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, 1);

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
		HAL_Delay(10);
	} else if (x >= L_JOY_LIMIT_X && x <= H_JOY_LIMIT_X)	//STAYING IN PLACE
	{
		//printf("STAYING IN PLACE \r\n");
		motor1->speed = 0;
		motor2->speed = motor1->speed;

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
	} else if (x > H_JOY_LIMIT_X)							//DRIVING BACKWARD
	{
		//printf("DRIVING BACKWARD \r\n");
		motor1->speed = B_FACTOR * x + B_CONST;
		motor2->speed = motor1->speed;

		motor1->IN_1 = 1;
		motor1->IN_2 = 0;
		motor2->IN_1 = 1;
		motor2->IN_2 = 0;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
	} else if (x < L_JOY_LIMIT_X)						//DRIVING FORWARD
	{
		//printf("DRIVING FORWARD \r\n");
		motor1->speed = F_FACTOR * x + F_CONST;
		motor2->speed = motor1->speed;

		motor1->IN_1 = 0;
		motor1->IN_2 = 1;
		motor2->IN_1 = 0;
		motor2->IN_2 = 1;

		HAL_GPIO_WritePin(DC_MOTOR_IN_1_GPIO_Port, DC_MOTOR_IN_1_Pin,
				motor1->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_2_GPIO_Port, DC_MOTOR_IN_2_Pin,
				motor1->IN_2);
		HAL_GPIO_WritePin(DC_MOTOR_IN_3_GPIO_Port, DC_MOTOR_IN_3_Pin,
				motor2->IN_1);
		HAL_GPIO_WritePin(DC_MOTOR_IN_4_GPIO_Port, DC_MOTOR_IN_4_Pin,
				motor2->IN_2);

		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_3, motor1->speed);
		__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_4, motor2->speed);
	}

}
